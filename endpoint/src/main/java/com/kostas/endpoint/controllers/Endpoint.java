package com.kostas.endpoint.controllers;

import com.kostas.endpoint.pojo.Singleton;
import com.kostas.endpoint.pojo.TestClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.TimeoutException;

@RestController
public class Endpoint {

    Logger logger = LoggerFactory.getLogger(Endpoint.class);

    @GetMapping("/test")
    ResponseEntity<Object> getTestNumberOne() {



            logger.info(String.valueOf(Singleton.getInstance().getTries()));

            System.out.println();
            return  ResponseEntity.status(200).build();


    }

    @RequestMapping(method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE, path = "/posttest")
    ResponseEntity<Object> getTestNumberTwo(@RequestBody TestClass testClass) throws TimeoutException {

        if (Singleton.getInstance().getTries() < 5) {
            logger.info(String.valueOf(Singleton.getInstance().getTries()));

            Singleton.getInstance().setTries(Singleton.getInstance().getTries() + 1);
            throw new TimeoutException("amas");
        }

            logger.info(testClass.getRandomString());

            System.out.println();
            return  ResponseEntity.status(200).build();


    }
}
