package com.kostas.endpoint.interceptor;

import com.kostas.endpoint.pojo.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

@Component
public class Intersept implements WebFilter {
    Logger logger = LoggerFactory.getLogger(Intersept.class);


    @Override
    public Mono<Void> filter(ServerWebExchange serverWebExchange, WebFilterChain webFilterChain) {
        if (Singleton.getInstance().getTries() < 5) {
            logger.info(String.valueOf(Singleton.getInstance().getTries()));

            Singleton.getInstance().setTries(Singleton.getInstance().getTries() + 1);
            return null;
        } else {
            Singleton.getInstance().setTries(0);

            logger.info(String.valueOf(Singleton.getInstance().getTries()));

            return webFilterChain.filter(serverWebExchange);

        }
    }
}
