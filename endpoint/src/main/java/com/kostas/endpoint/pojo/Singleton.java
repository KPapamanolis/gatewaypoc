package com.kostas.endpoint.pojo;

public class Singleton {
    public int getTries() {
        return tries;
    }

    public void setTries(int tries) {
        this.tries = tries;
    }
    private static Singleton singleton;

    private Singleton() {
    }

    public static Singleton getSingleton() {
        return singleton;
    }
    public static synchronized Singleton getInstance() {
        if (singleton == null) {
            singleton = new Singleton();
        }
        return singleton;
    }
    int tries=0;

}
