package com.kostas.gatewaypoc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.filter.factory.RetryGatewayFilterFactory;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.web.server.ServerWebExchange;
import reactor.retry.Repeat;
import reactor.retry.Retry;

import java.util.function.Consumer;

@SpringBootApplication
public class GatewaypocApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewaypocApplication.class, args);
    }

    Logger logger = LoggerFactory.getLogger(GatewaypocApplication.class);


    @Bean
    public RouteLocator customRouteLocator(RouteLocatorBuilder builder) {
        RetryGatewayFilterFactory.RetryConfig config=new RetryGatewayFilterFactory.RetryConfig();
        config.allMethods();
        config.setRetries(50);
        Consumer consumer= o -> {
      o=config;
        };
        return builder.routes().route(route ->
                route.path("/test").filters(v -> {
                    logger.info("try");
                    System.out.println("try");

                    return v.retry(101);
                }).uri("http://localhost:8081")
        )
                .route(route ->
                        route.path("/posttest").filters(f -> {
                            logger.info("try");

                            return f.retry( consumer );
                        }).uri("http://localhost:8081"))
                .route(route ->
                        route.path("/posttestom").filters(f -> {
                            logger.info("try");

                            return f.retry(2);
                        }).uri("http://localhost:8081"))
                .build();

    }
}
